﻿using Crud_MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Crud_MVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Crud_MVVM.View.Employee window = new View.Employee();
            EmployeeViewModel VM = new EmployeeViewModel();
            window.DataContext = VM;
            window.Show();
        }

    }
}
