﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Crud_MVVM.Model
{
    public class Employee:INotifyPropertyChanged
    {

        private int _id;
        private string _name ;
        private string _age;
        private string _company;
   

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged("ID");
            }
        }
        public string NAME
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("NAME");
            }
        }
        public string AGE
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
                OnPropertyChanged("AGE");
            }
        }
        public string COMPANY
        {
            get
            {
                return _company;
            }
            set
            {
                _company = value;
                OnPropertyChanged("COMPANY");
            }
        }
       

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
