﻿using MvvmCrudGv.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace Crud_MVVM.ViewModel
{
    public class EmployeeViewModel 
    {

        public ICommand Saveemployee { get; private set; }

         public EmployeeViewModel()
        {

            Saveemployee = new RelayCommand(saveemployee, canaddemployee);


        }

         public void saveemployee(object obj)
        {

            DataClasses1DataContext ob = new DataClasses1DataContext();

            tbl_Emp obtblemp = new tbl_Emp();
            var values = (object[])obj;

            obtblemp.name = values[0].ToString();
            obtblemp.age = Convert.ToInt32(values[1]);
            obtblemp.company = values[2].ToString();


            ob.tbl_Emps.InsertOnSubmit(obtblemp);
            ob.SubmitChanges();
          


        }

        public bool canaddemployee(object obj)
        {
            return (true);
        }




    }
}
